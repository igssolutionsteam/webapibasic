# Basic WebAPI Template #

Hi all, this is a web api template that you should use going forward for any webapi based project. When using Database first entity framework do not include the aspnet tables as this will cause confusion with the code first stuff which is being used to handle all the api authorization.

When creating new endpoints for apis if you can add them to the help section of the project so that we know where to go for our api and also it helps out mobile devs! :)

### How do I get set up? ###

* Clone this Repo
* Get the database from the DEV Local server called WebAPIBasic and restore it to your relevant project name
* Change your connection string to accomodate the initial catalogue change
* Build the solution and test.
* All done!

### Stuck? ###

* Grab Will/Tom or anyone else on the dev team